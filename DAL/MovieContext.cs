using Microsoft.EntityFrameworkCore;

public class MovieContext : DbContext
{
    public DbSet<Movie> Movies {get; set;}

    public readonly string DbPath;

    public MovieContext()
    {
        var folder = Environment.SpecialFolder.LocalApplicationData;
        var path = Environment.GetFolderPath(folder);
        DbPath = String.Join(path, "MovieDB.sqlite");
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
        optionsBuilder.UseSqlite($"DataSource={DbPath}");

}