public class MovieQuery
{
    public Movie GetMovie()
    {
        return new Movie{
            Id = Guid.NewGuid(),
            Name = "Terminator 2: Judgment Day",
            Year = "1991",
            Director = new Director{
                Id = Guid.NewGuid(),
                Name = "James Cameron"
            }
        };
    }
}