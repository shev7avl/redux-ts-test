public class Movie
{
    public Guid Id {get; set;}
    public string Name {get; set;}
    public string Year {get; set;}
    public Director Director {get; set;}
}